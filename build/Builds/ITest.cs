﻿using Nuke.Common;
using Nuke.Common.IO;
using Nuke.Common.Tools.DotNet;
using static Nuke.Common.Tools.DotNet.DotNetTasks;

namespace Builds;

public interface ITest : ICompile
{
    private AbsolutePath JUnitReportFile => OutputDirectory / "junit" / "{assembly}-test-result.xml";

    // ReSharper disable once UnusedMember.Global
    Target Test => _ => _
        .DependsOn(Compile)
        .Executes(() =>
        {
            DotNetTest(s => s
                .SetProjectFile(Solution)
                .SetConfiguration(Configuration)
                .AddLoggers($"junit;LogFilePath={JUnitReportFile};MethodFormat=Class;FailureBodyFormat=Verbose")
                .EnableNoRestore()
                .EnableNoBuild());
        });
}
