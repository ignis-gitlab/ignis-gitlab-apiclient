using System.IO;
using Nuke.Common;
using Nuke.Common.IO;
using Nuke.Common.Tooling;
using Nuke.Common.Tools.ReSharper;
using static Nuke.Common.Tools.ReSharper.ReSharperTasks;

namespace Builds;

public interface ILint : ICompile
{
    private AbsolutePath InspectCodeFile => OutputDirectory / "resharper" / "inspect-code.xml";
    private AbsolutePath CodeQualityFile => OutputDirectory / "resharper" / "code-quality.json";

    [PackageExecutable("resharper-to-codeclimate", "resharper-to-codeclimate.dll", Framework = "net6.0")]
    Tool ReSharperToCodeClimate => TryGetValue(() => ReSharperToCodeClimate);

    // ReSharper disable once UnusedMember.Global
    Target Lint => _ => _
        .DependsOn(Compile)
        .Executes(() =>
        {
            ReSharperInspectCode(s => s
                .SetTargetPath(Solution)
                .SetOutput(InspectCodeFile)
                .SetProperty("Configuration", Configuration)
                .SetProcessArgumentConfigurator(arguments => arguments
                    .Add("--no-build")));

            ReSharperToCodeClimate(new Arguments()
                .Add(InspectCodeFile)
                .Add(CodeQualityFile)
                .ToString());

            if (File.ReadAllText(InspectCodeFile).Contains("Severity=")) Assert.Fail("Found code problems.");
        });
}
